<?php 

// global panel options
global $foxtemas_options;

/*
    Redirecionamento Página inicial
*/
if($foxtemas_options['redirect_404'] == true) {

    header("HTTP/1.1 301 Moved Permanently");
    header("Location: ".get_bloginfo('url'));
    exit();

}

//   Vars 
// ==========================================================================
$title_404 = $foxtemas_options['title_404'];
$conteudo_404 = $foxtemas_options['conteudo_404'];

get_header(); ?>

<!-- section wrap -->
<section class="section-wrap">
    
    <!-- bg wrap -->
    <div class="bg-wrap clearfix">
        
        <!-- container -->
        <div class="container">
            
            <!-- row -->
            <div class="row">
                
                <!-- left content -->
                <div class="col-xs-12 col-sm-8 col-md-7">
                    <div class="left-content" role="main">
                        
                        <!-- article -->
                        <article class="article article-default">
                            
                            <!-- header article -->
                            <header class="header-article clearfix">
                                
                                <!-- name article -->
                                <h1 class="name-article text-center">
                                    <?php if($title_404) : echo $title_404; else : echo '404 Página não encontrada'; endif; ?>
                                </h1>
                                <!-- end name article -->

                            </header>
                            <!-- end header article -->

                            <!-- entry -->
                            <div class="entry clearfix">
                                <?php if($conteudo_404) : echo $conteudo_404; else : ?>

                                    <p class="text-center">
                                        <?php if($foxtemas_options['logo_header']['url']) : ?>
                                            <img src="<?php echo $foxtemas_options['logo_header']['url'];?>" alt="<?php bloginfo('name');?>">
                                        <?php else : ?>
                                            <img src="<?php echo FOXTEMAS_THEME_DIR . 'images/logo.png';?>" alt="<?php bloginfo('name');?>">
                                        <?php endif; ?>
                                    </p>

                                    <p class="text-center">
                                        Desculpe, mas a página que você tentou acessar não existe. <br>
                                        Ela pode ter sido movida, ou removida do site. Tente fazer uma busca novamente.
                                    </p>

                                <?php endif; ?>
                            </div>
                            <!-- end entry -->
                            
                        </article>
                        <!-- end article -->

                    </div>
                </div>
                <!-- end left content -->

                <!-- rigt content -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-md-offset-1">
                    <aside class="right-content">
                        <?php get_sidebar( 'sidebar' ); ?>
                    </aside>
                </div>
                <!-- end right content -->
                
            </div>
            <!-- end row -->

        </div>
        <!-- end container -->

    </div>
    <!-- end bg wrap -->

</section>
<!-- end section wrap -->

<?php get_footer();?>