<?php

/**
 *
 * Ajustes para funcionamento do plugin Advanced Custom Fields
 *
 *
 * @package Fox Temas Framework
 * @author Rhuan Carlos  <rhcarlosweb@gmail.com>
 * @version 2.0
 *
 *
 */


/**
// ==========================================================================
//   Define settings path
// ==========================================================================
**/
add_filter('acf/settings/path', 'my_acf_settings_path');
function my_acf_settings_path( $path ) {
	$path = FOXTEMAS_METAS_PATH;

	return $path;
}


/**
// ==========================================================================
//   Define settings dir
// ==========================================================================
**/
add_filter('acf/settings/dir', 'my_acf_settings_dir');
function my_acf_settings_dir( $dir ) {
	$dir = FOXTEMAS_METAS_DIR;

	return $dir;
}

/* Hide menu plugin */
// add_filter('acf/settings/show_admin', '__return_false');



/**
// ==========================================================================
//   JSON save fields
// ==========================================================================
**/
add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point( $path ) {

    // update path
    $path = FOXTEMAS_METAS_PATH . '/json';


    // return
    return $path;

}


/**
// ==========================================================================
//   JSON Load fields
// ==========================================================================
**/
add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {

    // remove original path (optional)
    unset($paths[0]);


    // append path
    $paths[] = FOXTEMAS_METAS_PATH . '/json';


    // return
    return $paths;

}

?>