<?php get_header(); ?>


<!-- section wrap -->
<section class="section-wrap">
    
    <!-- bg wrap -->
    <div class="bg-wrap clearfix">
        
        <!-- container -->
        <div class="container">
            
            <!-- row -->
            <div class="row">
                
                <!-- left content -->
                <div class="col-xs-12 col-sm-8 col-md-7">
                    <div class="left-content" role="main">
                        
                        <?php if (have_posts()) : ?>

                            <?php while (have_posts()) : the_post(); ?>
                            
                                <?php get_template_part( 'loops/loop', 'single' ); ?>

                            <?php endwhile; ?>
                            
                        <?php else : ?>
                        
                        <?php endif; ?>

                    </div>
                </div>
                <!-- end left content -->

                <!-- rigt content -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-md-offset-1">
                    <aside class="right-content">
                        <?php get_sidebar( 'sidebar' ); ?>
                    </aside>
                </div>
                <!-- end right content -->
                
            </div>
            <!-- end row -->

        </div>
        <!-- end container -->

    </div>
    <!-- end bg wrap -->

</section>
<!-- end section wrap -->
<?php get_footer();?>