<?php

/** 
// ==========================================================================
//   Fox Temas Styles
// ==========================================================================
**/
function foxtemas_styles() 
	{	
        // bootstrap
        wp_enqueue_style( 'bootstrap', FOXTEMAS_THEME_DIR . 'css/bootstrap.css', false, null);
        
		// Font Awesome
		wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', false, null);
		
		// Style Geral
		$ver = filemtime( get_template_directory() . '/style.css');
		wp_enqueue_style('style', FOXTEMAS_THEME_DIR . 'style.css', false, $ver, null);
	}
	add_action('wp_enqueue_scripts', 'foxtemas_styles', 100);
?>