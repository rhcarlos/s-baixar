<?php 
/**
 * Modelo de loop padrão para páginas
 *
 * @package Só Baixar
 * @version 1.0 
 * 
 */
?>

<!-- article -->
<article class="article article-default">
    
    <!-- header article -->
    <header class="header-article clearfix">
        
        <!-- name article -->
        <h1 class="name-article">
            <?php the_title();?>
        </h1>
        <!-- end name article -->

    </header>
    <!-- end header article -->

    <!-- entry -->
    <div class="entry clearfix">
        <?php the_content('[ + ] Expandir Postagem ››'); ?>
    </div>
    <!-- end entry -->
    
</article>
<!-- end article -->