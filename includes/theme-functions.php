<?php 
// ==========================================================================
//   File Security Check
// ==========================================================================// 
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'Você não tem permissão suficiente para acessar esse arquivo!' );
}



//   Global Panel Options
// ==========================================================================
global $foxtemas_options;



/**
 * Fix menu bug to chrome version 46
 */
add_action('admin_enqueue_scripts', 'chrome_fix');
function chrome_fix() {
    if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'Chrome' ) !== false )
        wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0); }' );
}




/** 
// ==========================================================================
//   Clean Itens Header
// ==========================================================================
**/
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);





/** 
// ==========================================================================
//   Style personalizado editor wordpress
// ==========================================================================
**/
$ver_editor = filemtime( get_template_directory() . '/style.css');
add_editor_style(FOXTEMAS_THEME_DIR .'css/editor-style.css?ver='.$ver_editor );





/** 
// ==========================================================================
//   CSS Personalizado
// ==========================================================================
**/
function foxtemas_custom_css() {
	   
    // global panel options
	global $foxtemas_options;

    /**
     * CSs adicional
     */
	if($foxtemas_options['ativar_css'] == '1') {
		echo '<style>';
		echo $foxtemas_options['custom_css'];
		echo '</style>';
	}

    /**
     * Se fundo topo personalizado é igual a gradient
     */
    if($foxtemas_options['type_bg_header'] == 'gradient') { ?>
        
    <style>
        .header {
            background: <?php echo $foxtemas_options['bg_gradient']['from'];?>;
            background: -moz-linear-gradient(top,  <?php echo $foxtemas_options['bg_gradient']['from'];?> 0%, <?php echo $foxtemas_options['bg_gradient']['to'];?> 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,<?php echo $foxtemas_options['bg_gradient']['from'];?>), color-stop(100%,<?php echo $foxtemas_options['bg_gradient']['to'];?>));
            background: -webkit-linear-gradient(top,  <?php echo $foxtemas_options['bg_gradient']['from'];?> 0%,<?php echo $foxtemas_options['bg_gradient']['to'];?> 100%);
            background: -o-linear-gradient(top,  <?php echo $foxtemas_options['bg_gradient']['from'];?> 0%,<?php echo $foxtemas_options['bg_gradient']['to'];?> 100%);
            background: -ms-linear-gradient(top,  <?php echo $foxtemas_options['bg_gradient']['from'];?> 0%,<?php echo $foxtemas_options['bg_gradient']['to'];?> 100%);
            background: linear-gradient(to bottom,  <?php echo $foxtemas_options['bg_gradient']['from'];?> 0%,<?php echo $foxtemas_options['bg_gradient']['to'];?> 100%);
        }        
    </style>

    <?php }

}





/** 
// ==========================================================================
//   Favicons
// ==========================================================================
**/
function foxtemas_favicons() {

    // global panel options
	global $foxtemas_options;

    //   Vars 
    // ==========================================================================
	$favicon = $foxtemas_options['favicon']['url'];

	// conditional
	if($favicon) { ?>
        <link rel="icon" type="image/png" href="<?php echo $favicon;?>" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo $favicon;?>" sizes="194x194">
        <link rel="icon" type="image/png" href="<?php echo $favicon;?>" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo $favicon;?>" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo $favicon;?>" sizes="16x16">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo $favicon;?>">
        <meta name="theme-color" content="#ffffff">

    <?php }
}





/** 
// ==========================================================================
//   Remove Recent comments Style
// ==========================================================================
**/
function my_remove_recent_comments_style() {
    // global wp widget factory
	global $wp_widget_factory;

	remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}
add_action('widgets_init', 'my_remove_recent_comments_style');





/** 
// ==========================================================================
//   Font sizes editor wordpress
// ==========================================================================
**/

//   Enable font size & font family selects in the editor 
// ==========================================================================
if ( ! function_exists( 'wpex_mce_buttons' ) ) {
	function wpex_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'fontsizeselect' ); // Add Font Size Select
		return $buttons;
	}
}
add_filter( 'mce_buttons_2', 'wpex_mce_buttons' );


//   Customize mce editor font sizes 
// ==========================================================================
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
	function wpex_mce_text_sizes( $initArray ){
		$initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 18px 21px 24px 28px 32px 36px";
		return $initArray;
	}
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );



/** 
// ==========================================================================
//   Theme Support
// ==========================================================================
**/
add_theme_support( 'post-thumbnails' ); 





/** 
// ==========================================================================
//   Image Sizes
// ==========================================================================
**/
add_image_size('acf', 30, 30, true);
add_image_size('thumb_top_10', 75, 110, true);
add_image_size( 'thumb_relacionados', 170, 125, true );



/** 
// ==========================================================================
//   First Image Post
// ==========================================================================
**/
function foxtemas_first_img() {
    // global post
	global $post;


	$first_img = '';
	ob_start();
	ob_end_clean();
	$output    = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	
	if($output >= 1) {
		$first_img = $matches[1][0];
	}

	return $first_img;
}




/** 
// ==========================================================================
//   Custom Function Thumbnail
// ==========================================================================
**/
function foxtemas_thumbnail($size = 'medium', $url = false) {
    global $post;

    if($url == true) {
        // 
        // MOSTRA APENAS URL DA IMAGEM DO POST
        // 

        // thumbnail url
        $thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id(), $size);
        $thumbnail_url = $thumbnail_url[0];

        if(has_post_thumbnail()) {
            echo $thumbnail_url;
        } elseif(foxtemas_first_img()) {
            echo foxtemas_first_img();
        } else {
            echo 'http://placehold.it/300&text=Sem Imagem';
        }

    } else {
        // 
        // MOSTRA TAG IMG COM URL
        // 

        if(has_post_thumbnail()) {
            the_post_thumbnail($size); 
        } 
        elseif(foxtemas_first_img()) {
            echo '<img src="'.foxtemas_first_img().'" alt="imagem post">';
        } else {
            echo '<img src="http://placehold.it/300&text=Sem Imagem" alt="sem imagem">';
        }

    }
}




/** 
// ==========================================================================
//   Resumo Content
// ==========================================================================
**/
function foxtemas_resumo($content = null, $limit = null, $link = false) {

    // global post
    global $post;

    /*
        @content: var content
        @limit: number
    */

    if($link == true) {

        //   Resumo com link leia mais 
        // ==========================================================================

        if(strlen(strip_tags($content)) > $limit) {
            echo substr(strip_tags($content), 0, $limit) .' <a href="'.get_permalink().'">[leia mais]</a>';
        } else {
            echo strip_tags($content);
        }

    } else {

        //   Resumo sem link 
        // ==========================================================================

        if(strlen(strip_tags($content)) > $limit) {
            echo substr(strip_tags($content), 0, $limit) .' ...';
        } else {
            echo strip_tags($content);
        }

    }
}




/** 
// ==========================================================================
//   Pagination
// ==========================================================================
**/
function foxtemas_pagenavi() {
    // global query
	global $wp_query, $wp_rewrite;


	$pages = '';
	$max   = $wp_query->max_num_pages;
	if (!$current = get_query_var('paged'))
	$current = 1;
	$a['base']      = str_replace(999999999, '%#%', get_pagenum_link(999999999));
	$a['total']     = $max;
	$a['current']   = $current;
	$total          = 0;
	$a['mid_size']  = 3;
	$a['end_size']  = 1;
	$a['prev_text'] = '<span class="number">« Anterior</span>';
	$a['next_text'] = '<span class="number">Próxima »</span>';
	$a['show_all']  = false;
	$a['before_page_number'] = '<span class="number">';
	$a['after_page_number'] = '</span>';


	if ($max > 1) {
		if($total == 1) {
			$pages = '<span class="pages">Página ' . $current . ' de ' . $max . '</span>';
		}
		echo '<div class="pagenavi">'.$pages . paginate_links($a).'<div class="clear"></div></div>';
	}
}


/** 
// ==========================================================================
//   Remove Widgets desnecessárias
// ==========================================================================
**/
function remove_calendar_widget() {
    unregister_widget('WP_Widget_Search');
}

add_action( 'widgets_init', 'remove_calendar_widget' );



/** 
// ==========================================================================
//   Custom CSS
// ==========================================================================
**/
function foxtemas_admin_css() {
    ?>

    <style type="text/css">
        /* Width Size column */
        div#widgets-right .sidebars-column-1, div#widgets-right .sidebars-column-2 {
            width: 100% !important;
            max-width: none;
        }

        /* Description widget field */
        #widgets-right .widgets-holder-wrap .description {
            padding-left: 0;
        }
    </style>

    <?php 
}

add_action( 'admin_head', 'foxtemas_admin_css');


/** 
// ==========================================================================
//   Fox Temas Breadcrumb
// ==========================================================================
**/
function foxtemas_breadcrumb() {
    /* === OPTIONS === */
    $text['home']     = 'Página Inicial'; // text for the 'Home' link
    $text['category'] = '%s'; // text for a category page
    $text['search']   = 'Você pesquisou por "%s"'; // text for a search results page
    $text['tag']      = '%s'; // text for a tag page
    $text['author']   = 'Artigo postado por %s'; // text for an author page
    $text['404']      = 'Erro 404'; // text for the 404 page
    $text['page']     = 'Página %s'; // text 'Page N'
    $text['cpage']    = 'Comentário Página %s'; // text 'Comment Page N'
    $delimiter      = '›'; // delimiter between crumbs
    $delim_before   = '<span class="divider">'; // tag before delimiter
    $delim_after    = '</span>'; // tag after delimiter
    $show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show
    $show_on_home   = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $show_current   = 1; // 1 - show current page title, 0 - don't show
    $show_title     = 1; // 1 - show the title for the links, 0 - don't show
    $before         = '<span class="current">'; // tag before the current crumb
    $after          = '</span>'; // tag after the current crumb
    /* === END OF OPTIONS === */
    global $post;
    $home_link      = home_url('/');
    $link_before    = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
    $link_after     = '</span>';
    $link_attr      = ' itemprop="url"';
    $link_in_before = '<span itemprop="title">';
    $link_in_after  = '</span>';
    $link           = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
    $frontpage_id   = get_option('page_on_front');

    // condicional remove erro to search page
    if(!is_search()) {
        $parent_id      = $post->post_parent;
    }

    $delimiter      = ' ' . $delim_before . $delimiter . $delim_after . ' ';
    if (is_home() || is_front_page()) {
        if ($show_on_home == 1) echo '<div class="breadcrumbs"><a href="' . $home_link . '">' . $text['home'] . '</a></div>';
    } else {
        echo '<div class="breadcrumbs">';
        if ($show_home_link == 1) echo sprintf($link, $home_link, $text['home']);
        if ( is_category() ) {
            $cat = get_category(get_query_var('cat'), false);
            if ($cat->parent != 0) {
                $cats = get_category_parents($cat->parent, TRUE, $delimiter);
                $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                if ($show_home_link == 1) echo $delimiter;
                echo $cats;
            }
            if ( get_query_var('paged') ) {
                $cat = $cat->cat_ID;
                echo $delimiter . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $delimiter . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current == 1) echo $delimiter . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
            }
        } elseif ( is_search() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo $before . sprintf($text['search'], get_search_query()) . $after;
        } elseif ( is_day() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F')) . $delimiter;
            echo $before . get_the_time('d') . $after;
        } elseif ( is_month() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo $before . get_the_time('F') . $after;
        } elseif ( is_year() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo $before . get_the_time('Y') . $after;
        } elseif ( is_single() && !is_attachment() ) {
            if ($show_home_link == 1) echo $delimiter;
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
                if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $delimiter);
                if ($show_current == 0 || get_query_var('cpage')) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
                if ( get_query_var('cpage') ) {
                    echo $delimiter . sprintf($link, get_permalink(), get_the_title()) . $delimiter . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
                } else {
                    if ($show_current == 1) echo $before . get_the_title() . $after;
                }
            }
        // custom post type
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            $post_type = get_post_type_object(get_post_type());
            if ( get_query_var('paged') ) {
                echo $delimiter . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $delimiter . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current == 1) echo $delimiter . $before . $post_type->label . $after;
            }
        } elseif ( is_attachment() ) {
            if ($show_home_link == 1) echo $delimiter;
            $parent = get_post($parent_id);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            if ($cat) {
                $cats = get_category_parents($cat, TRUE, $delimiter);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
            }
            printf($link, get_permalink($parent), $parent->post_title);
            if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
        } elseif ( is_page() && !$parent_id ) {
            if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
        } elseif ( is_page() && $parent_id ) {
            if ($show_home_link == 1) echo $delimiter;
            if ($parent_id != $frontpage_id) {
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    if ($parent_id != $frontpage_id) {
                        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                    }
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                    echo $breadcrumbs[$i];
                    if ($i != count($breadcrumbs)-1) echo $delimiter;
                }
            }
            if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
        } elseif ( is_tag() ) {
            if ($show_current == 1) echo $delimiter . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
        } elseif ( is_author() ) {
            if ($show_home_link == 1) echo $delimiter;
            global $author;
            $author = get_userdata($author);
            echo $before . sprintf($text['author'], $author->display_name) . $after;
        } elseif ( is_404() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo $before . $text['404'] . $after;
        } elseif ( has_post_format() && !is_singular() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo get_post_format_string( get_post_format() );
        }
        echo '</div><!-- .breadcrumbs -->';
    }
} // end foxtemas_breadcrumb()



/** 
// ==========================================================================
//   Relacionados
// ==========================================================================
**/
function foxtemas_relacionados() {
    // global 
    global $post;
    
    $orig_post = $post;
    $categories = get_the_category($post->ID);
    
    // if posts in same category of current post
    if ($categories) {
        $category_ids = array();
        foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    
        $args=array(
            'category__in' => $category_ids,
            'post__not_in' => array($post->ID),
            'posts_per_page'=> 3, 
            'orderby' => 'rand'
        );
    
        $my_query = new wp_query( $args );
    
        if( $my_query->have_posts() ) { ?>
    
            <!-- relacionados -->
            <div class="relacionados clearfix">
    
                <span class="title-heading">
                    Relacionados
                </span>
                
                <!-- lista relacionados -->
                <div class="row lista-relacionados">
    
                <?php 
                    while( $my_query->have_posts() ) { 
                    $my_query->the_post();?>
            
                        <div class="col-xs-4">
                            <a href="<?php the_permalink();?>"  class="thumb-related">
                                <?php foxtemas_thumbnail('thumb_relacionados'); ?>
                            </a>

                            <a class="title-related" href="<?php the_permalink();?>">
                                <?php the_title(); ?>
                            </a>
                        </div>
            
                    <?php } // end while ?>
    
                </div>
                <!-- end lista relacionados -->
    
            </div>
            <!-- end relacionados -->
    
        <?php } // end if query
    
    } // end if have posts
    
    $post = $orig_post;
    wp_reset_query(); 
}

?>