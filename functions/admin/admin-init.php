<?php

// Load the embedded Redux Framework
if( file_exists( dirname( __FILE__ ) . '/redux-framework/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/redux-framework/framework.php' );
} 
elseif( file_exists( dirname( __FILE__ ) . '/redux-framework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/redux-framework/ReduxCore/framework.php' );
}

// Load the theme/plugin options
if( file_exists( dirname( __FILE__ ) . '/options/options-init.php' ) ) {
    require_once( dirname( __FILE__ ) . '/options/options-init.php' );
}


/** 
 // ==========================================================================
 //   Css
 // ==========================================================================
 */
function addPanelCSS()
{
    
    // ==========================================================================
    //   CSS
    // ==========================================================================
    // Font Awesome
    wp_enqueue_style( 'font-awesome', FOXTEMAS_THEME_DIR . 'includes/integrations/font-awesome/css/font-awesome.min.css', false, null );
    
    // custom css
    wp_register_style( 'foxtemas-panel-custom-css', FOXTEMAS_PANEL_DIR . 'options/css/foxtemas-panel-custom.css', array(
        'redux-admin-css'
    ) , time() , 'all' );
    wp_enqueue_style( 'foxtemas-panel-custom-css' );
}

add_action( 'redux/page/foxtemas_options/enqueue', 'addPanelCSS' );
