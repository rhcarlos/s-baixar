<?php
if (!class_exists('Redux')) {
    return;
}

// This is your option name where all the Redux data is stored.
$opt_name = "foxtemas_options";

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 *
 */

$theme = wp_get_theme();

// For use with some settings. Not necessary.

$args = array(
    'opt_name' => 'foxtemas_options',
    'display_name' => '<a href="http://www.foxtemas.com.br" target="_blank"><img src="' . FOXTEMAS_PANEL_DIR . 'options/assets/logo-fox.png" style="width: 100%; height: auto;"></a>',
    'display_version' => '2.0',
    'page_slug' => 'painel_foxtemas',
    'page_title' => 'Painel Fox Temas',
    'update_notice' => FALSE,
    'intro_text' => 'Veja mais temas disponíveis em nossa loja <a href="http://www.foxtemas.com.br" target="_blank" title="Fox Temas - Temas para WordPress">www.foxtemas.com.br</a>',
    
    // 'footer_text' => '',
    'footer_credit' => 'Para mais temas visite nossa loja <a href="http://www.foxtemas.com.br" target="_blank" title="Fox Temas - Templates para WordPress">Fox Temas</a>',
    'admin_bar' => TRUE,
    'menu_type' => 'menu',
    'menu_title' => 'Painel Fox Temas',
    'menu_icon' => FOXTEMAS_PANEL_DIR . 'options/assets/fox-icon.png',
    'allow_sub_menu' => TRUE,
    'page_parent_post_type' => '',
    'customizer' => TRUE,
    'default_mark' => '*',
    'google_api_key' => '629b439893e9b135517ea903fe2747ba127c500e',
    'class' => 'foxtemas_panel_css',
    'hints' => array(
        'icon' => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color' => '#1e73be',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'light',
            'rounded' => '1',
            'style' => 'tipsy',
        ) ,
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ) ,
        'tip_effect' => array(
            'show' => array(
                'effect' => 'fade',
                'duration' => '500',
                'event' => 'mouseover',
            ) ,
            'hide' => array(
                'effect' => 'fade',
                'duration' => '500',
                'event' => 'mouseleave unfocus',
            ) ,
        ) ,
    ) ,
    'output' => TRUE,
    'output_tag' => TRUE,
    'compiler' => TRUE,
    'page_permissions' => 'manage_options',
    'show_import_export' => TRUE,
    'transient_time' => '3600',
    'network_sites' => TRUE,
    'disable_tracking' => TRUE,
    'dev_mode' => FALSE,
);

// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
$args['share_icons'][] = array(
    'url' => 'https://www.facebook.com/foxtemas',
    'title' => 'Curtir Página',
    'icon' => 'fa fa-facebook-square'
);
$args['share_icons'][] = array(
    'url' => 'http://twitter.com/foxtemas',
    'title' => 'Seguir Twitter',
    'icon' => 'fa fa-twitter'
);
$args['share_icons'][] = array(
    'url' => 'http://www.foxtemas.com.br',
    'title' => 'Loja Virtual',
    'icon' => 'fa fa-shopping-cart'
);

Redux::setArgs($opt_name, $args);

/*
 * ---> END ARGUMENTS
*/

/*
 * ---> START HELP TABS
*/

$tabs = array();
Redux::setHelpTab($opt_name, $tabs);

// Set the help sidebar
$content = __('<p>This is the sidebar content, HTML is allowed.</p>', 'admin_folder');
Redux::setHelpSidebar($opt_name, $content);

/*
 * <--- END HELP TABS
*/

/*
 *
 * ---> START SECTIONS
 *
*/

/** 
 // ==========================================================================
 //   Geral
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Geral', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_geral',
    'icon' => 'fa fa-home',
    'subsection' => false,
    'fields' => array(
        
        // Favicons
        array(
            'id' => 'favicon',
            'type' => 'media',
            'url' => true,
            'title' => __('Favicon', 'foxtemas_theme') ,
            'desc' => __('Recomendado tamanho de 48x48', 'foxtemas_theme') ,
            'subtitle' => __('Carregue um favicon para o site.', 'foxtemas_theme') ,
            'readonly' => false,
            'placeholder' => __('Favicon não selecionado', 'foxtemas_theme') ,
        ) ,
        
        // Logo
        array(
            'id' => 'logo_header',
            'type' => 'media',
            'url' => true,
            'title' => __('Logo', 'foxtemas_theme') ,
            'desc' => __('Tamanho recomendado de largura 225px', 'foxtemas_theme') ,
            'subtitle' => __('Carregue uma logo para substituir pela logo original do site', 'foxtemas_theme') ,
            'readonly' => false,
            'placeholder' => __('Logo não selecionada', 'foxtemas_theme') ,
        ) ,
        
        // tipografia geral
        array(
            'id' => 'geral_body',
            'type' => 'typography',
            'title' => __('Tipografia Geral', 'foxtemas_theme') ,
            'google' => true,
            'font-backup' => true,
            'output' => array(
                'body'
            ) ,
            'units' => 'px',
            'subtitle' => __('Troque a font padrão do site (Roboto) por qualquer uma outra da lista.', 'foxtemas_theme') ,
            'font-weight' => false,
            'text-align' => false,
            'color' => false,
            'font-size' => false,
            'line-height' => false,
            'font-style' => false,
            'default' => array() ,
        ) ,
        
        // Relatório de erros
        array(
            'id' => 'relatorio_erro',
            'type' => 'text',
            'title' => __('Shortcode relatório de erros', 'foxtemas_theme') ,
            'subtitle' => __('Insira o código do formulário de relatório de erros criado pelo Contact Form 7', 'foxtemas_theme') ,
            'desc' => __('', 'foxtemas_theme') ,
            'validate' => '',
            'msg' => '',
            'placeholder' => '[contact-form-7 id="999" title="Contato"]',
            'default' => ''
        ) ,
        
        // Códigos antes do </head>
        array(
            'id' => 'before_head',
            'type' => 'ace_editor',
            'title' => __('Códigos adicionais', 'foxtemas_theme') ,
            'subtitle' => __('Insira códigos adicionais que serão inseridos antes do fechamento da tag <code>/head</code>', 'foxtemas_theme') ,
            'mode' => 'html',
            'theme' => 'monokai',
            'desc' => '',
            'default' => ""
        ) ,
        
        // Google analyctis
        array(
            'id' => 'tracking_code',
            'type' => 'ace_editor',
            'title' => __('Código de Rastreamento Google Analytics', 'foxtemas_theme') ,
            'subtitle' => __('Insira o código de rastreamento do Google Analytics, ou qualquer outro script que seja exibido no fim do carregamento do site', 'foxtemas_theme') ,
            'mode' => 'html',
            'theme' => 'monokai',
            'desc' => '',
            'default' => ""
        ) ,
    )
));

/** 
 // ==========================================================================
 //   Background
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Styles', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_background',
    'icon' => 'fa fa-paint-brush',
    'subsection' => false,
    'fields' => array(
        
        // background
        array(
            'type' => 'background',
            'id' => 'background',
            'title' => __('Background', 'foxtemas_theme') ,
            'subtitle' => __('Personalize seu fundo como quiser', 'foxtemas_theme') ,
            'desc' => __('', 'foxtemas_theme') ,
            'output' => array(
                'body'
            ) ,
            'background-color' => true,
            'background-repeat' => true,
            'background-attachment' => true,
            'background-position' => true,
            'background-image' => true,
            'background-clip' => false,
            'background-origin' => false,
            'background-size' => true,
            'preview_media' => true,
            'preview' => true,
            'preview_height' => '300px',
            'transparent' => true,
            'default' => array() ,
        ) ,
        
        // bordas conteudo
        array(
            'id' => 'border_wrap',
            'type' => 'color',
            'title' => __('Cor Borda Conteúdo', 'foxtemas_theme') ,
            'subtitle' => __('Substitua a cor da borda do conteúdo', 'foxtemas_theme') ,
            'default' => '',
            'validate' => 'color',
            'transparent' => true,
            'output' => array(
                'border-color' => '.bg-breadcrumb, .bg-ads, .bg-wrap'
            ) ,
        ) ,
    ) ,
));

/** 
 // ==========================================================================
 //   Topo
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Topo', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_top',
    'icon' => 'fa fa-arrow-up',
    'subsection' => false,
    'fields' => array(
        
        // Opções de fundo menu
        array(
            'type' => 'button_set',
            'id' => 'type_bg_header',
            'title' => __('Tipo Fundo Topo', 'foxtemas_theme') ,
            'subtitle' => __('Selecione o tipo de fundo que será usado no topo do site', 'foxtemas_theme') ,
            'desc' => __('', 'foxtemas_theme') ,
            
            'options' => array(
                'padrao' => 'Padrão',
                'gradient' => 'Gradiente',
                'imagem' => 'Imagem',
            ) ,
            'multi' => false,
            'default' => 'padrao',
        ) ,
        
        // gradient option
        array(
            'id' => 'bg_gradient',
            'type' => 'color_gradient',
            'title' => __('Fundo gradient para o topo', 'foxtemas_theme') ,
            'subtitle' => __('Selecione as cores gradients de cima para baixo que será usado como fundo no menu', 'foxtemas_theme') ,
            'desc' => __('', 'foxtemas_theme') ,
            'validate' => 'color',
            'default' => array(
                'from' => '',
                'to' => '',
            ) ,
            'required' => array(
                'type_bg_header',
                'equals',
                'gradient'
            ) ,
        ) ,
        
        // imagem
        array(
            'type' => 'background',
            'id' => 'bg_header',
            'title' => __('Fundo imagem para o topo', 'foxtemas_theme') ,
            'subtitle' => __('Insira um fundo personalizado para o topo do site', 'foxtemas_theme') ,
            'desc' => __('', 'foxtemas_theme') ,
            'output' => array(
                '.header'
            ) ,
            'background-color' => true,
            'background-repeat' => true,
            'background-attachment' => false,
            'background-position' => true,
            'background-image' => true,
            'background-clip' => false,
            'background-size' => false,
            'preview_media' => true,
            'preview' => true,
            'preview_height' => '85px',
            'transparent' => true,
            'default' => array() ,
            'required' => array(
                'type_bg_header',
                'equals',
                'imagem'
            ) ,
        ) ,
    ) ,
));

/** 
 // ==========================================================================
 //   Artigo
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Artigo', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_article',
    'icon' => 'fa fa-file',
    'subsection' => false,
    'fields' => array(
        
        // Tipografia
        array(
            'id' => 'typo_entry',
            'type' => 'typography',
            'title' => __('Tipografia artigo', 'foxtemas_theme') ,
            'google' => true,
            'font-backup' => true,
            'word-spacing' => true,
            'letter-spacing' => true,
            'output' => array(
                '.entry'
            ) ,
            'units' => 'px',
            'subtitle' => __('Ajuste da tipografia do artigo', 'foxtemas_theme') ,
            'default' => array() ,
        ) ,
    ) ,
));

/** 
 // ==========================================================================
 //   Rodapé
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Rodapé', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_footer',
    'icon' => 'fa fa-arrow-down',
    'subsection' => false,
    'fields' => array(
        
        // texto rodapé
        array(
            'id' => 'texto_rodape',
            'type' => 'text',
            'title' => __('Texto rodapé', 'foxtemas_theme') ,
            'subtitle' => __('Texto personalizado no rodapé', 'foxtemas_theme') ,
            'desc' => __('', 'foxtemas_theme') ,
            'validate' => '',
            'msg' => '',
            'placeholder' => __('©2015 - Só Baixar | Downloads Grátis', 'foxtemas_theme') ,
            'default' => ''
        ) ,
    ) ,
));

/** 
 // ==========================================================================
 //   Feedburner
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Feedburner', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_rss',
    'icon' => 'fa fa-rss',
    'subsection' => false,
    'fields' => array(
        
        array(
            'id' => 'feed',
            'type' => 'text',
            'title' => __('FeedBurner', 'foxtemas_theme') ,
            'subtitle' => __('Insira apenas o <strong>ID</strong> do feedburner', 'foxtemas_theme') ,
            'desc' => __('Usado para registro de newsletter pelo feedburner', 'foxtemas_theme') ,
            'validate' => '',
            'msg' => '',
            'placeholder' => 'rhcarlosweb',
            'default' => ''
        ) ,
    ) ,
));

/** 
 // ==========================================================================
 //   Publicidades
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Publicidades', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_ads',
    'icon' => 'fa fa-money',
    'subsection' => false,
    'fields' => array() ,
));

/** 
 // ==========================================================================
 //   Topo
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Topo', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_ads_topo',
    'subsection' => true,
    'fields' => array(
        
        // texto topo
        array(
            'type' => 'ace_editor',
            'id' => 'texto_topo',
            'title' => __('Texto Topo', 'foxtemas_theme') ,
            'subtitle' => __('Insira um texto personalizado que será inserido abaixo do topo do site', 'foxtemas_theme') ,
            'mode' => '',
            'theme' => 'monokai',
            'desc' => __('', 'foxtemas_theme') ,
        ) ,
        
        // ads
        array(
            'type' => 'ace_editor',
            'id' => 'ads_header',
            'title' => __('Publicidade Topo', 'foxtemas_theme') ,
            'subtitle' => __('Insira um banner que será mostrado no topo do site', 'foxtemas_theme') ,
            'mode' => 'html',
            'theme' => 'monokai',
            'desc' => __('Sugerido que seja inserido um banner em imagem, ou um script de banner <strong> responsivo</strong>', 'foxtemas_theme') ,
        ) ,
    ) ,
));

/** 
 // ==========================================================================
 //   Artigo
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Artigo', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_ads_artigo',
    'subsection' => true,
    'fields' => array(
        
        // ads
        array(
            'type' => 'ace_editor',
            'id' => 'ads_relacionados',
            'title' => __('Publicidade Acima Relacionados', 'foxtemas_theme') ,
            'subtitle' => __('Insira um banner que será mostrado acima dos posts relacionados', 'foxtemas_theme') ,
            'mode' => 'html',
            'theme' => 'monokai',
            'desc' => __('Sugerido que seja inserido um banner em imagem, ou um script de banner <strong> responsivo</strong>', 'foxtemas_theme') ,
        ) ,
    ) ,
));

/** 
 // ==========================================================================
 //   Páginas Não encontradas
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Páginas Erro', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_not_found',
    'icon' => 'fa fa-minus-square',
    'subsection' => false,
    'fields' => array()
));

/** 
 // ==========================================================================
 //   Página 404
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Página 404', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_404',
    'subsection' => true,
    'fields' => array(
        
        // redirect
        array(
            'id' => 'redirect_404',
            'type' => 'switch',
            'title' => 'Redirecionar Página 404',
            'subtitle' => 'Ative para redirecionar todas as páginas 404 para sua página inicial. Algumas pessoas acham que é bom para SEO.',
            'default' => true,
            'on' => 'Ativar',
            'off' => 'Desativar',
        ) ,
        
        // title
        array(
            'id' => 'title_404',
            'type' => 'text',
            'title' => 'Título Página 404',
            'subtitle' => 'Insira um título personalizado para página 404',
            'desc' => '',
            'validate' => '',
            'msg' => '',
            'default' => '',
            'placeholder' => '',
            'required' => array(
                'redirect_404',
                'equals',
                '0'
            ) ,
        ) ,
        
        //  Conteudo
        array(
            'id' => 'conteudo_404',
            'type' => 'editor',
            'title' => 'Conteúdo Página 404',
            'subtitle' => 'Insira um conteúdo personalizado para a página 404',
            'default' => '',
            'args' => array(
                'teeny' => false,
                'textarea_rows' => 15
            ) ,
            'required' => array(
                'redirect_404',
                'equals',
                '0'
            ) ,
        ) ,
    )
));

/** 
 // ==========================================================================
 //   Busca
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('Busca', 'foxtemas_theme') ,
    'desc' => __('Detalhes do conteúdo mostrado quando uma busca retorna nenhum resultado', 'foxtemas_theme') ,
    'id' => 'section_search_not_found',
    'subsection' => true,
    'fields' => array(
        
        // titulo
        array(
            'id' => 'search_404_title',
            'type' => 'text',
            'title' => __('Título', 'foxtemas_theme') ,
            'subtitle' => __('Substitua o título padrão de uma busca sem resultados', 'foxtemas_theme') ,
            'desc' => __('', 'foxtemas_theme') ,
            'validate' => '',
            'msg' => '',
            'placeholder' => '<i class="fa fa-search"></i> Busca não encontrada',
            'default' => ''
        ) ,
        
        // conteudo pagina resutlado busca
        array(
            'id' => 'search_404_content',
            'type' => 'editor',
            'title' => __('Conteúdo', 'foxtemas_theme') ,
            'subtitle' => __('Substitua o conteúdo quando uma busca não retorna resultados', 'foxtemas_theme') ,
            'default' => '',
            'args' => array(
                'teeny' => false,
                'textarea_rows' => 10,
                'wpautop' => true,
                'media_buttons' => true,
            ) ,
        ) ,
    ) ,
));

/** 
 // ==========================================================================
 //   CSS Personalizado
 // ==========================================================================
 *
 */
Redux::setSection($opt_name, array(
    'title' => __('CSS Personalizado', 'foxtemas_theme') ,
    'desc' => __('', 'foxtemas_theme') ,
    'id' => 'section_css',
    'icon' => 'fa fa-css3',
    'subsection' => false,
    'fields' => array(
        
        // switch css
        array(
            'id' => 'ativar_css',
            'type' => 'switch',
            'title' => 'Ativar CSS Personalizado',
            'subtitle' => 'Ative o css personalizado para o site',
            'default' => false,
            'on' => 'Ativar',
            'off' => 'Desativar',
        ) ,
        
        // custm scs
        array(
            'id' => 'custom_css',
            'type' => 'ace_editor',
            'title' => 'CSS Personalizado',
            'subtitle' => 'Cole seu código CSS personalizado',
            'mode' => 'css',
            'theme' => 'monokai',
            'desc' => '',
            'default' => '',
            'required' => array(
                'ativar_css',
                'equals',
                '1'
            )
        ) ,
    )
));

/*
 * <--- END SECTIONS
*/
