<?php 
// ==========================================================================
//   File Security Check
// ==========================================================================// 
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'Você não tem permissão suficiente para acessar esse arquivo!' );
}




/**
 * Widget Feedburner
 *
 * @return Mostra formulário de assinatura via feedburner
 */
class FoxTemasFeedBurner extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'feedburner', // Base ID
            __( '#Só Baixar: Feedburner', 'foxtemas_theme' ), // Name
            array( 'description' => __( 'Mostra formulário de feedburner', 'foxtemas_theme' ), ) // Args
        );
    }

    /**
     * Front-end exibição estrutura
     */
    public function widget( $args, $instance ) {

        /**
         * Global functions panel
         */
        global $foxtemas_options;

        /**
         * Return id of widget
         * @var string
         */
        $widget_id = $args['widget_id'];

        /**
         * Return id of sidebar 
         * @var string
         */
        $sidebar_id = $args['id'];

        /**
         * Add class to widgets footer
         * @var string
         */
        $class_footer = '';

        if( ($sidebar_id == 'sidebar-footer-left') || ($sidebar_id == 'sidebar-footer-center') || ($sidebar_id == 'sidebar-footer-right') ) {
            $class_footer = 'widget-footer';
        }

        ?>

        <li class="widget <?php echo $class_footer;?> widget_feedburner">
            
            <?php
            /**
             * Condicional para evitar o uso da widget nas sidebars menores
             */
            if($sidebar_id == 'sidebar-small-left' || $sidebar_id == 'sidebar-small-right') { ?>
                <div class="alert alert-danger"><i class="fa fa-close"></i> Widget não suportada nessa sidebar, tente usar ela em outra sidebar.</div>
            <?php } 
            /**
             * Se não for as widgets menores
             * Executar função normal da widget
             */
            else {?>

                <?php if(get_field('titulo', 'widget_'.$widget_id)) : ?>
                    <!-- widgettitle -->
                    <h3 class="widgettitle">
                        <span>
                            <?php the_field('icone', 'widget_'.$widget_id); ?> <?php the_field('titulo', 'widget_'.$widget_id); ?>
                        </span>
                    </h3>
                    <!-- end widgettitle -->
                <?php endif; ?>

                <!-- sidebar content -->
                <div class="sidebar-content-feedburner">
                    
                    <?php if(get_field('texto_feedburner', 'widget_'.$widget_id)) : ?>
                        <?php the_field('texto_feedburner', 'widget_'.$widget_id); ?>
                    <?php else : ?>
                        Receba nossos filmes e atualizações diretamente em seu email. É simples, rápido e grátis. Basta cadastrar seu email aqui:
                    <?php endif; ?>

                    <!-- form feedburner -->
                    <div class="form-feedburner">
                        <form action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=<?php echo $foxtemas_options['feed'];?>', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
                            <input type="hidden" name="loc" value="pt_BR"/>
                            <input type="hidden" value="<?php echo $foxtemas_options['feed'];?>" name="uri"/>
                            <input type="text" class="campo_feed" name="email" value="Digite seu email..." onclick="if (this.value == 'Digite seu email...') { this.value = ''; }" onBlur="if (this.value == '') { this.value = 'Digite seu email...'; }"/>
                            <input type="submit" value="CADASTRAR" class="botao_feed" />
                        </form>
                    </div>
                    <!-- end form feedburner -->

                </div>
                <!-- end sidebar content -->

            <?php } ?>

        </li>

        <?php
    }

    /**
     * Back-end infos
     */
    public function form( $instance ) {
        ?>

            <p>
                Configurações
            </p>

        <?php
    }

    /**
     * Update info
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();

        return $instance;
    }

} // class FoxTemasFeedBurner

add_action( 'widgets_init', function(){
     register_widget( 'FoxTemasFeedBurner' );
});




/**
 * Widget de Busca pesronalizada
 *
 * @return Mostra formulário personalizado de busca para o tema Só Baixar
 */
class FoxTemasBusca extends WP_Widget {
    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'foxtemas_search', // Base ID
            __( '#Só Baixar: Busca', 'foxtemas_theme' ), // Name
            array( 'description' => __( 'Mostra Formulário de Busca', 'foxtemas_theme' ), ) // Args
        );
    }

    /**
     * Content Front-end
     */
    public function widget( $args, $instance ) {

        /**
         * Return id of sidebar 
         * @var string
         */
        $sidebar_id = $args['id'];

        /**
         * Add class to widgets footer
         * @var string
         */
        $class_footer = '';

        if( ($sidebar_id == 'sidebar-footer-left') || ($sidebar_id == 'sidebar-footer-center') || ($sidebar_id == 'sidebar-footer-right') ) {
            $class_footer = 'widget-footer';
        }

        ?>
        <li class="widget <?php echo $class_footer;?> widget_custom_search">
            <!-- search form -->
            <div class="searchform-foxtemas">
                <form method="get" action="<?php bloginfo('url');?>/">
                    <input class="pesquisa"  type="text" value="Faça sua busca..." name="s"  size="10"  onclick="if (this.value == 'Faça sua busca...') { this.value = ''; }" onblur="if (this.value == '') { this.value = 'Faça sua busca...'; }" />
                    <button type="submit" class="button-search"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- end searchform -->
        </li>
        <?php 
    }

    /**
     * Form backend
     */
    public function form( $instance ) {
        ?>
            <p>
                Configurações
            </p>
        <?php 
    }

    /**
     * Update infos          
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        return $instance;
    }

} // class FoxTemasBusca

// Register Widget
add_action( 'widgets_init', function(){
     register_widget( 'FoxTemasBusca' );
});



/**
 * Widget personalizada
 *
 * @return Widget personalizada para publicação de publicidades, banners, entre outros, 
 *         uma widget que permite ser inserida sem título e não deixa erro nas widgets posteriores
 */
class FoxTemasPublicidade extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'widget_ads', // Base ID
            __( '#Só Baixar: Widget Personalizada', 'foxtemas_theme' ), // Name
            array( 'description' => __( 'Widget personalizada para postar conteúdo sem título ou com título e ícone. Use essa widget ao invés de usar uma widget de texto sem título.', 'foxtemas_theme' ), ) // Args
        );
    }


    /**
     * Content Front-end
     */
    public function widget( $args, $instance ) {

        /**
         * Return id of widget
         * @var string
         */
        $widget_id = $args['widget_id'];

        ?>
            <li class="widget clearfix">

                <?php if(get_field('titulo', 'widget_'.$widget_id)) : ?>
                    <!-- widgettitle -->
                    <h3 class="widgettitle">
                        <span>
                            <?php the_field('icone', 'widget_'.$widget_id); ?> <?php the_field('titulo', 'widget_'.$widget_id); ?>
                        </span>
                    </h3>
                    <!-- end widgettitle -->
                <?php endif; ?>

                <!-- sidebar custom content -->
                <div class="sidebar-content sidebar-custom-content">
                    <?php the_field('conteudo_widget', 'widget_'.$widget_id); ?>
                </div>
                <!-- end sidebar custom content -->
            
            </li>
        <?php 
    }

    /**
     * Form backend
     */
    public function form( $instance ) {
        ?>
        <p>
            Configurações
        </p>
        <?php 
    }

    /**
     * Update infos          
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        return $instance;
    }

} // class FoxTemasPublicidade

// Register Widget
add_action( 'widgets_init', function(){
     register_widget( 'FoxTemasPublicidade' );
});




/**
 * Widget top 10    
 *
 * @return Retorna lista collapse de top 10 
 */
class FoxTemasTop10 extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'widget_top10', // Base ID
            __( '#Só Baixar: Top 10', 'foxtemas_theme' ), // Name
            array( 'description' => __( 'Mostra lista personalizada de top 10 que se expande', 'foxtemas_theme' ), ) // Args
        );
    }


    /**
     * Content Front-end
     */
    public function widget( $args, $instance ) {

        /**
         * Global functions panel
         */
        global $foxtemas_options;

        /**
         * Return id of widget
         * @var string
         */
        $widget_id = $args['widget_id'];

        /**
         * Return id of sidebar 
         * @var string
         */
        $sidebar_id = $args['id'];

        /**
         * Add class to widgets footer
         * @var string
         */
        $class_footer = '';

        if( ($sidebar_id == 'sidebar-footer-left') || ($sidebar_id == 'sidebar-footer-center') || ($sidebar_id == 'sidebar-footer-right') ) {
            $class_footer = 'widget-footer';
        }

        /**
         * Tipo de exibição de artigos top 10
         * @var string
         */
        $modo_exibicao = get_field('modo_exibicao', 'widget_'.$widget_id);

        ?>
            <!-- widget top 10 -->
            <li class="widget <?php echo $class_footer;?> widget_top10">


            <?php
            /**
             * Condicional para evitar o uso da widget nas sidebars menores
             */
            if($sidebar_id == 'sidebar-small-left' || $sidebar_id == 'sidebar-small-right') { ?>
                <div class="alert alert-danger"><i class="fa fa-close"></i> Widget não suportada nessa sidebar, tente usar ela em outra sidebar.</div>
            <?php } 
            /**
             * Se não for as widgets menores
             * Executar função normal da widget
             */
            else {?>

                <?php if(get_field('titulo', 'widget_'.$widget_id)) : ?>
                    <!-- widgettitle -->
                    <h3 class="widgettitle">
                        <span>
                            <?php the_field('icone', 'widget_'.$widget_id); ?> <?php the_field('titulo', 'widget_'.$widget_id); ?>
                        </span>
                    </h3>
                    <!-- end widgettitle -->
                <?php endif; ?>
                
                <!-- list top 10 -->
                <div class="list-top-10" id="collapse-<?php echo $widget_id;?>" role="tablist">
                    
                    <?php 
                    /**
                     * Modo de exibição manual
                     *
                     * Artigos selecionados através da widget 
                     * @var string
                     */
                    if($modo_exibicao == 'manual') { ?>

                        <?php 
                        /**
                         * Global $post infos
                         */
                        global $post;

                        /**
                         * Puxa relação de posts selecionados na widget
                         * @var object
                         */
                        $posts_top_10 = get_field('posts_top_10', 'widget_'.$widget_id);

                        /**
                         * Se posts selecionados    
                         */
                        if( $posts_top_10 ): $count_top_10 = 1; ?>

                            <?php foreach( $posts_top_10 as $post):  ?>

                                <?php setup_postdata($post); ?>

                                <?php 
                                /**
                                 * Se post primeiro
                                 */
                                if($count_top_10 == 1) : ?>

                                    <!-- panel -->
                                    <div class="panel item-top-10">

                                        <!-- heading panel -->
                                        <div class="heading-top-10 clearfix" role="tab">
                                            <a role="button" data-toggle="collapse" class="open-top-10" data-parent="#collapse-<?php echo $widget_id;?>" href="#top-10-<?php echo $count_top_10;?>-<?php echo $widget_id;?>" aria-expanded="true">
                                                <!-- num top 10 -->
                                                <span class="num-top-10">
                                                    <?php echo $count_top_10;?>
                                                </span>
                                                <!-- end num top 10 -->

                                                <!-- name heading -->
                                                <span class="name-heading">
                                                    <?php the_title();?>
                                                </span>
                                                <!-- end name heading -->
                                            </a>
                                        </div>
                                        <!-- end heading panel -->
                                        
                                        <!-- content top 10 -->
                                        <div id="top-10-<?php echo $count_top_10;?>-<?php echo $widget_id;?>" class="expand-content-top-10 collapse in" role="tabpanel">
                                            <div class="content-top-10 clearfix">

                                                <!-- thumb top 10 -->
                                                <div class="thumb-top-10">
                                                    <a href="<?php the_permalink();?>">
                                                        <?php foxtemas_thumbnail('thumb_top_10'); ?>
                                                    </a>
                                                </div>
                                                <!-- end thumb top 10 -->

                                                <!-- details top 10 -->
                                                <div class="details-top-10">
                                                    <!-- name top 10 -->
                                                    <h4 class="name-top-10">
                                                        <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                                            <?php the_title(); ?>
                                                        </a>
                                                    </h4>
                                                    <!-- end name top 10 -->

                                                    <!-- cats top 10 -->
                                                    <div class="cats-top-10">
                                                        Categoria: <?php the_category( ', ' );?>
                                                    </div>
                                                    <!-- end cats top 10 -->

                                                    <!-- show details -->
                                                    <div class="show-details-top-10">
                                                        <a href="<?php the_permalink();?>" title="Ver Detalhes">
                                                            Ver Detalhes
                                                        </a>
                                                    </div>
                                                    <!-- end show details -->
                                                </div>
                                                <!-- end details top 10 -->
                                                    
                                            </div>
                                        </div>
                                        <!-- end content top 10 -->

                                    </div>
                                    <!-- end panel -->

                                <?php else : ?>

                                    <!-- panel -->
                                    <div class="panel item-top-10">

                                        <!-- heading panel -->
                                        <div class="heading-top-10 clearfix" role="tab">
                                            <a role="button" data-toggle="collapse" class="open-top-10 collapsed" data-parent="#collapse-<?php echo $widget_id;?>" href="#top-10-<?php echo $count_top_10;?>-<?php echo $widget_id;?>" aria-expanded="false">
                                                <!-- num top 10 -->
                                                <span class="num-top-10">
                                                    <?php echo $count_top_10;?>
                                                </span>
                                                <!-- end num top 10 -->

                                                <!-- name heading -->
                                                <span class="name-heading">
                                                    <?php the_title();?>
                                                </span>
                                                <!-- end name heading -->
                                            </a>
                                        </div>
                                        <!-- end heading panel -->
                                        
                                        <!-- content top 10 -->
                                        <div id="top-10-<?php echo $count_top_10;?>-<?php echo $widget_id;?>" class="expand-content-top-10 collapse" role="tabpanel">
                                            <div class="content-top-10 clearfix">

                                                <!-- thumb top 10 -->
                                                <div class="thumb-top-10">
                                                    <a href="<?php the_permalink();?>">
                                                        <?php foxtemas_thumbnail('thumb_top_10'); ?>
                                                    </a>
                                                </div>
                                                <!-- end thumb top 10 -->

                                                <!-- details top 10 -->
                                                <div class="details-top-10">
                                                    <!-- name top 10 -->
                                                    <h4 class="name-top-10">
                                                        <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                                            <?php the_title(); ?>
                                                        </a>
                                                    </h4>
                                                    <!-- end name top 10 -->

                                                    <!-- cats top 10 -->
                                                    <div class="cats-top-10">
                                                        Categoria: <?php the_category( ', ' );?>
                                                    </div>
                                                    <!-- end cats top 10 -->

                                                    <!-- show details -->
                                                    <div class="show-details-top-10">
                                                        <a href="<?php the_permalink();?>" title="Ver Detalhes">
                                                            Ver Detalhes
                                                        </a>
                                                    </div>
                                                    <!-- end show details -->
                                                </div>
                                                <!-- end details top 10 -->
                                                    
                                            </div>
                                        </div>
                                        <!-- end content top 10 -->

                                    </div>
                                    <!-- end panel -->

                                <?php endif; ?>


                            <?php $count_top_10++; endforeach; ?>

                            <?php wp_reset_postdata(); wp_reset_query(); ?>

                        <?php endif; ?>

                    <?php } 
                    /**
                     * Se não for post manual exibir loop padrão WordPress
                     */
                    else { 

                        /**
                         * Get today dates
                         * @var array
                         */
                        $today = getdate();
                        ?>

                        <?php /**
                         * Mostra posts mais visualizados
                         *
                         * Necessário instalar plugin WP-Postviews
                         * @var string
                         */
                        if($modo_exibicao == 'most_viewed') { ?>

                            <?php 
                            /**
                             * Loop Personalizado
                             */
                            $loop_top_10 = array(
                                'post_type' => 'post',
                                'showposts' => 10,
                                'meta_key' => 'views', 
                                'orderby' => 'meta_value_num', 
                                'order' => 'DESC',
                                'year' => $today['year'],
                                // 'monthnum' => $today['mon'],
                            );

                            $the_query = new WP_Query( $loop_top_10 ); ?>

                        <?php } 
                        /**
                         * Mostra posts mais votados
                         *
                         * Necessário instalar plugin WP-Postratings
                         * @var string
                         */
                        else if($modo_exibicao == 'most_rated') { ?>

                            <?php 
                            /**
                             * Loop Personalizado
                             */
                            $loop_top_10 = array(
                                'post_type' => 'post',
                                'showposts' => 10,
                                'meta_key' => 'ratings_users', 
                                'orderby' => 'meta_value_num', 
                                'order' => 'DESC',
                                'year' => $today['year'],
                                // 'monthnum' => $today['mon'],
                            );

                            $the_query = new WP_Query( $loop_top_10 ); ?>

                        <?php } 
                        /**
                         * Mostra posts mais bem avaliados
                         *
                         * Necessário instalar plugin WP-Postratings
                         * @var [type]
                         */
                        else if($modo_exibicao == 'highest_score') { ?>

                            <?php 
                            /**
                             * Loop Personalizado
                             */
                            $loop_top_10 = array(
                                'post_type' => 'post',
                                'showposts' => 10,
                                'meta_key' => 'ratings_average', 
                                'orderby' => 'meta_value_num', 
                                'order' => 'DESC',
                                'year' => $today['year'],
                                // 'monthnum' => $today['mon'],
                            );

                            $the_query = new WP_Query( $loop_top_10 ); ?>

                        <?php } ?>

                        <?php 
                        /**
                         * Loop padrão WordPress
                         */
                        if ( $the_query->have_posts() ) : $count_top_10 = 1; ?>

                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <?php 
                                /**
                                 * Se post primeiro
                                 */
                                if($count_top_10 == 1) : ?>

                                    <!-- panel -->
                                    <div class="panel item-top-10">

                                        <!-- heading panel -->
                                        <div class="heading-top-10 clearfix" role="tab">
                                            <a role="button" data-toggle="collapse" class="open-top-10" data-parent="#collapse-<?php echo $widget_id;?>" href="#top-10-<?php echo $count_top_10;?>-<?php echo $widget_id;?>" aria-expanded="true">
                                                <!-- num top 10 -->
                                                <span class="num-top-10">
                                                    <?php echo $count_top_10;?>
                                                </span>
                                                <!-- end num top 10 -->

                                                <!-- name heading -->
                                                <span class="name-heading">
                                                    <?php the_title();?>
                                                </span>
                                                <!-- end name heading -->
                                            </a>
                                        </div>
                                        <!-- end heading panel -->
                                        
                                        <!-- content top 10 -->
                                        <div id="top-10-<?php echo $count_top_10;?>-<?php echo $widget_id;?>" class="expand-content-top-10 collapse in" role="tabpanel">
                                            <div class="content-top-10 clearfix">

                                                <!-- thumb top 10 -->
                                                <div class="thumb-top-10">
                                                    <a href="<?php the_permalink();?>">
                                                        <?php foxtemas_thumbnail('thumb_top_10'); ?>
                                                    </a>
                                                </div>
                                                <!-- end thumb top 10 -->

                                                <!-- details top 10 -->
                                                <div class="details-top-10">
                                                    <!-- name top 10 -->
                                                    <h4 class="name-top-10">
                                                        <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                                            <?php the_title(); ?>
                                                        </a>
                                                    </h4>
                                                    <!-- end name top 10 -->

                                                    <!-- cats top 10 -->
                                                    <div class="cats-top-10">
                                                        Categoria: <?php the_category( ', ' );?>
                                                    </div>
                                                    <!-- end cats top 10 -->

                                                    <!-- show details -->
                                                    <div class="show-details-top-10">
                                                        <a href="<?php the_permalink();?>" title="Ver Detalhes">
                                                            Ver Detalhes
                                                        </a>
                                                    </div>
                                                    <!-- end show details -->
                                                </div>
                                                <!-- end details top 10 -->
                                                    
                                            </div>
                                        </div>
                                        <!-- end content top 10 -->

                                    </div>
                                    <!-- end panel -->

                                <?php else : ?>

                                    <!-- panel -->
                                    <div class="panel item-top-10">

                                        <!-- heading panel -->
                                        <div class="heading-top-10 clearfix" role="tab">
                                            <a role="button" data-toggle="collapse" class="open-top-10 collapsed" data-parent="#collapse-<?php echo $widget_id;?>" href="#top-10-<?php echo $count_top_10;?>-<?php echo $widget_id;?>" aria-expanded="false">
                                                <!-- num top 10 -->
                                                <span class="num-top-10">
                                                    <?php echo $count_top_10;?>
                                                </span>
                                                <!-- end num top 10 -->

                                                <!-- name heading -->
                                                <span class="name-heading">
                                                    <?php the_title();?>
                                                </span>
                                                <!-- end name heading -->
                                            </a>
                                        </div>
                                        <!-- end heading panel -->
                                        
                                        <!-- content top 10 -->
                                        <div id="top-10-<?php echo $count_top_10;?>-<?php echo $widget_id;?>" class="expand-content-top-10 collapse" role="tabpanel">
                                            <div class="content-top-10 clearfix">

                                                <!-- thumb top 10 -->
                                                <div class="thumb-top-10">
                                                    <a href="<?php the_permalink();?>">
                                                        <?php foxtemas_thumbnail('thumb_top_10'); ?>
                                                    </a>
                                                </div>
                                                <!-- end thumb top 10 -->

                                                <!-- details top 10 -->
                                                <div class="details-top-10">
                                                    <!-- name top 10 -->
                                                    <h4 class="name-top-10">
                                                        <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                                            <?php the_title(); ?>
                                                        </a>
                                                    </h4>
                                                    <!-- end name top 10 -->

                                                    <!-- cats top 10 -->
                                                    <div class="cats-top-10">
                                                        Categoria: <?php the_category( ', ' );?>
                                                    </div>
                                                    <!-- end cats top 10 -->

                                                    <!-- show details -->
                                                    <div class="show-details-top-10">
                                                        <a href="<?php the_permalink();?>" title="Ver Detalhes">
                                                            Ver Detalhes
                                                        </a>
                                                    </div>
                                                    <!-- end show details -->
                                                </div>
                                                <!-- end details top 10 -->
                                                    
                                            </div>
                                        </div>
                                        <!-- end content top 10 -->

                                    </div>
                                    <!-- end panel -->

                                <?php endif; ?>
                            
                            <?php $count_top_10++; endwhile; ?>

                            <?php wp_reset_postdata(); wp_reset_query(); ?>

                        <?php else: endif; ?>


                    <?php } 
                    /**
                     * Fim modo de exibição manual e condicional padrão
                     */
                    ?>

                </div>
                <!-- end list top 10 -->

            <?php } ?>

            </li>
            <!-- end widget top 10 -->

        <?php 
    }

    /**
     * Form backend
     */
    public function form( $instance ) {
        
        ?>
        <p>
            Configurações
        </p>
        <?php 
    }

    /**
     * Update infos          
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        return $instance;
    }

} // class FoxTemasTop10

// Register Widget
add_action( 'widgets_init', function(){
    register_widget( 'FoxTemasTop10' );
});




/**
 * Widget de Destaques
 *
 * Mostra campo repetidor para quadros de Destaques
 */
class FoxTemasDestaques extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'widget_destaques', // Base ID
            __( '#Só Baixar: Destaques', 'foxtemas_theme' ), // Name
            array( 'description' => __( 'Mostra widget personalizada para posts destaques', 'foxtemas_theme' ), ) // Args
        );
    }


    /**
     * Content Front-end
     */
    public function widget( $args, $instance ) {

        /**
         * Return id of widget
         * @var string
         */
        $widget_id = $args['widget_id'];

        /**
         * Return id of sidebar 
         * @var string
         */
        $sidebar_id = $args['id'];

        /**
         * Add class to widgets footer
         * @var string
         */
        $class_footer = '';

        if( ($sidebar_id == 'sidebar-footer-left') || ($sidebar_id == 'sidebar-footer-center') || ($sidebar_id == 'sidebar-footer-right') ) {
            $class_footer = 'widget-footer';
        }

        ?>

        <li class="widget <?php echo $class_footer;?> widget_featured">
            
            <?php if(get_field('titulo', 'widget_'.$widget_id)) : ?>
                <!-- widgettitle -->
                <h3 class="widgettitle">
                    <span>
                        <?php the_field('icone', 'widget_'.$widget_id); ?> <?php the_field('titulo', 'widget_'.$widget_id); ?>
                    </span>
                </h3>
                <!-- end widgettitle -->
            <?php endif; ?>
        
            <!-- sidebar content -->
            <div class="sidebar-content">
                
                <?php 
                if( have_rows('destaques', 'widget_'.$widget_id) ): ?>
                
                    <?php
                    while ( have_rows('destaques', 'widget_'.$widget_id) ) : the_row(); ?>
                
                        <!-- item destaque -->
                        <div class="item-destaque-widget">
                            <?php if(get_sub_field('url')) : ?>
                                <a href="<?php the_sub_field('url');?>" title="<?php the_sub_field('titulo');?>">
                            <?php endif; ?>

                                <img src="<?php the_sub_field('imagem');?>" alt="Imagem <?php the_sub_field('titulo');?>">
                                
                            <?php if(get_sub_field('url')) : ?>
                                </a>
                            <?php endif; ?>

                            <!-- name destaque -->
                            <div class="name-destaque-widget">
                                <?php if(get_sub_field('url')) : ?>
                                    <a href="<?php the_sub_field('url');?>" title="<?php the_sub_field('titulo');?>">
                                <?php endif; ?>
                                    <?php the_sub_field('titulo');?>
                                <?php if(get_sub_field('url')) : ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                            <!-- end name destaque -->
                        </div>
                        <!-- end item destaque -->
                
                    <?php endwhile; ?>
                
                <?php else : endif;
                ?>

            </div>
            <!-- end sibar content -->

        </li>


        <?php 
    }

    /**
     * Form backend
     */
    public function form( $instance ) {
        
        ?>
        <p>
            Configurações
        </p>
        <?php 
    }

    /**
     * Update infos          
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        return $instance;
    }

} // class FoxTemasDestaques

// Register Widget
add_action( 'widgets_init', function(){
    register_widget( 'FoxTemasDestaques' );
});




/**
 * Widget com tabs
 *
 * Mostra widget com tabs personalizadas, podendo adicionar qualquer tipo de conteúdo
 */
class FoxTemasWidgetTabs extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'widget_tabs', // Base ID
            __( '#Só Baixar: Widget Tabs', 'foxtemas_theme' ), // Name
            array( 'description' => __( 'Mostra widget com tabs', 'foxtemas_theme' ), ) // Args
        );
    }


    /**
     * Content Front-end
     */
    public function widget( $args, $instance ) {

        /**
         * Global functions panel
         */
        global $foxtemas_options;

        /**
         * Return id of widget
         * @var string
         */
        $widget_id = $args['widget_id'];

        /**
         * Return id of sidebar 
         * @var string
         */
        $sidebar_id = $args['id'];

        /**
         * Add class to widgets footer
         * @var string
         */
        $class_footer = '';

        if( ($sidebar_id == 'sidebar-footer-left') || ($sidebar_id == 'sidebar-footer-center') || ($sidebar_id == 'sidebar-footer-right') ) {
            $class_footer = 'widget-footer';
        }

        ?>

        <li class="widget <?php echo $class_footer;?> widget_tabs">
            
            <!-- tab block -->
            <div class="tabBlock">

                <?php 
                /**
                 * Titulo Tabs
                 */
                if( have_rows('tabs', 'widget_'.$widget_id) ): $count_tab = 1; ?>

                    <!-- title tabs -->
                    <ul class="tabBlock-tabs">
                    <?php while ( have_rows('tabs', 'widget_'.$widget_id) ) : the_row();
                        
                        /**
                         * Se houver tab
                         */
                        if( get_row_layout() == 'tab' ): ?>

                            <li class="tabBlock-tab <?php if($count_tab == 1) : echo 'is-active'; endif;?>"><?php the_sub_field('titulo');?></li>

                        <?php endif;
                        // end se houver tab
                
                    $count_tab++; endwhile; ?>
                    </ul>
                    <!-- end title tabs -->
                    
                <?php else : endif;
                // end Tabs
                ?>

                <?php 
                /**
                 * Conteudo tabs
                 */
                if( have_rows('tabs', 'widget_'.$widget_id) ): ?>

                    <!-- content tabs -->
                    <div class="tabBlock-content">
                    <?php while ( have_rows('tabs', 'widget_'.$widget_id) ) : the_row();
                        
                        /**
                         * Se houver tab
                         */
                        if( get_row_layout() == 'tab' ): 

                            /**
                             * Modo de exibição da tab
                             * @var string
                             */
                            $options_content_tab = get_sub_field('options_content_tab');

                        ?>
                            
                            <!-- tabBlock pane -->
                            <div class="tabBlock-pane">
                                <div class="sidebar-content">

                                    <?php 
                                    /**
                                     * Se tipo de conteudo é editor de texto
                                     */
                                    if($options_content_tab == 'editor_texto') : ?>
                                        <?php the_sub_field('content_tab'); ?>
                                    <?php endif; ?>


                                    <?php
                                    /**
                                     * Se modo de exibição é lista de categoria
                                     */
                                    if($options_content_tab == 'lista_categoria') : 

                                        /**
                                         * Categorias
                                         * @var array
                                         */
                                        $categoria_tab = get_sub_field('categoria_tab');

                                        /**
                                         * Se houver categorias selecionadas
                                         */
                                        if($categoria_tab) {

                                            /**
                                             * Leitura do array
                                             * @var array
                                             */
                                            $copy = $categoria_tab;

                                            /**
                                             * Variável para receber ids
                                             * @var string
                                             */
                                            $todas_categorias = '';

                                            /**
                                             * Extrair IDs do array
                                             */
                                            foreach ($categoria_tab as $cat_id) {

                                                // Separador
                                                if (next($copy )) {
                                                    $separator = ', '; // Add comma for all elements instead of last
                                                } else {
                                                    $separator = '';
                                                }

                                                // junta todas os ids em uma string só
                                                $todas_categorias .= $cat_id.''.$separator;
                                            }
                                        } else {
                                            $todas_categorias = '';
                                        }

                                        /**
                                         * Número de posts 
                                         * @var int
                                         */
                                        $num_posts = get_sub_field('num_posts');
                                        if(!$num_posts) {
                                            $num_posts = 5;
                                        }
                                    ?>

                                    <?php                                     
                                    /**
                                     * Loop Personalizado
                                     */
                                    $loop_lista_tabs = array(
                                        'post_type' => 'post',
                                        'showposts' => $num_posts,
                                        'cat' => $todas_categorias,
                                    );
                                    
                                    
                                    $loop_lista = new WP_Query( $loop_lista_tabs ); ?>
                                    
                                    <?php if ( $loop_lista->have_posts() ) : ?>

                                        <ul>                                    
                                            <?php while ( $loop_lista->have_posts() ) : $loop_lista->the_post(); ?>
                                        
                                                <li>
                                                    <a href="<?php the_permalink();?>">
                                                        <?php the_title(); ?>
                                                    </a>
                                                </li>
                                            
                                            <?php endwhile; ?>
                                        </ul>

                                        <?php wp_reset_postdata(); wp_reset_query(); ?>
                                    
                                    <?php else:  ?>
                                        <p>
                                            <?php _e('Nenhum resultado encontrado', 'foxtemas_theme');?>
                                        </p>
                                    <?php endif;  endif; ?>

                                </div>
                            </div>
                            <!-- end tabBlock pane -->

                        <?php endif;
                        // end se houver tab
                
                    endwhile; ?>
                    </div>
                    <!-- end content tabs -->
                    
                <?php else : endif;
                // end Tabs
                ?>

            </div>
            <!-- end tablock -->


        </li>

        <?php 
    }

    /**
     * Form backend
     */
    public function form( $instance ) {
        
        ?>
        <p>
            Configurações
        </p>
        <?php 
    }

    /**
     * Update infos          
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        return $instance;
    }

} // class FoxTemasWidgetTabs

// Register Widget
add_action( 'widgets_init', function(){
    register_widget( 'FoxTemasWidgetTabs' );
});


/**
 * Widget Redes Sociais
 *
 * Mostra quadros de ícones
 */
class WidgetSocial extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'widget_redes_sociais', // Base ID
            __( '#Só Baixar: Redes Sociais', 'foxtemas_theme' ), // Name
            array( 'description' => __( 'Mostra ícones de redes sociais', 'foxtemas_theme' ), ) // Args
        );
    }


    /**
     * Content Front-end
     */
    public function widget( $args, $instance ) {

        /**
         * Global functions panel
         */
        global $foxtemas_options;

        /**
         * Return id of widget
         * @var string
         */
        $widget_id = $args['widget_id'];

        /**
         * Return id of sidebar 
         * @var string
         */
        $sidebar_id = $args['id'];

        ?>

        <li class="widget widget_social">
            
            <!-- redes widget -->
            <div class="redes-widget">
                
                <?php if( have_rows('redes_sociais', 'widget_'.$widget_id) ): ?>
                
                    <?php while ( have_rows('redes_sociais', 'widget_'.$widget_id) ) : the_row(); 

                        /**
                         * Código de cor
                         * @var string
                         */
                        $cor_fundo = get_sub_field('fundo');
                        if($cor_fundo) {
                            $cor_fundo = 'style="background-color:'.$cor_fundo.';"';
                        } else {
                            $cor_fundo = '';
                        }

                    ?>
                
                        <!-- redes item widget -->
                        <div class="redes-item-widget">
                            <a href="<?php the_sub_field('url');?>" title="<?php the_sub_field('nome');?>" <?php echo $cor_fundo;?> class="square-social" target="_blank">
                                <?php the_sub_field('icon'); ?>
                            </a>
                        </div>
                        <!-- end redes item widget -->
                
                    <?php endwhile; ?>
                
                <?php else : endif;?>

            </div>
            <!-- end redes widget -->
        
        </li>


        <?php 
    }

    /**
     * Form backend
     */
    public function form( $instance ) {
        
        ?>
        <p>
            Configurações
        </p>
        <?php 
    }

    /**
     * Update infos          
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        return $instance;
    }

} // class WidgetSocial

// Register Widget
add_action( 'widgets_init', function(){
    register_widget( 'WidgetSocial' );
});



?>