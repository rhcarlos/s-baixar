<!-- sidebar big 1 -->
<div class="sidebar">
    <ul>            
        <?php dynamic_sidebar('sidebar-big-one');?>
    </ul>
</div>
<!-- end sidebar big one -->


<!-- sidebar smalls -->
<div class="sidebar-small">
    <div class="row">
        
        <!-- sidebar small left -->
        <div class="col-xs-6">

            <div class="sidebar">
                <ul>            
                    <?php dynamic_sidebar('sidebar-small-left');?>
                </ul>
            </div>
            
        </div>
        <!-- sidebar small left -->

        <!-- sidebar small right -->
        <div class="col-xs-6">
            
            <div class="sidebar">
                <ul>            
                    <?php dynamic_sidebar('sidebar-small-right');?>
                </ul>
            </div>

        </div>
        <!-- end sidebar small right -->

    </div>
</div>
<!-- end sidebar smalls -->

<!-- sidebar big 2 -->
<div class="sidebar">
    <ul>            
        <?php dynamic_sidebar('sidebar-big-two');?>
    </ul>
</div>
<!-- end sidebar big 2 -->

