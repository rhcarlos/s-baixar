<?php 

/** 
// ==========================================================================
//   Fox Temas Javascripts
// ==========================================================================
**/
function foxtemas_javascripts() 
	{	
		// superfish
		wp_enqueue_script( 'hoverIntent', FOXTEMAS_THEME_DIR.'includes/js/hoverIntent.js', array('jquery'), null, true );
		wp_enqueue_script( 'superfish', FOXTEMAS_THEME_DIR.'includes/js/superfish.min.js', array('jquery'), null, true );

        // bootstrap
        wp_enqueue_script( 'bootstrap', FOXTEMAS_THEME_DIR.'includes/js/bootstrap.js', array('jquery'), null, true );

		// main
        wp_enqueue_script( 'main', FOXTEMAS_THEME_DIR.'includes/js/main.js', array('jquery'), null, true );

        $ver = filemtime( get_template_directory() . '/includes/js/main.js');
        wp_enqueue_script( 'main', FOXTEMAS_THEME_DIR.'includes/js/main.js', array('jquery'), $ver, true );


	}
	add_action('wp_enqueue_scripts', 'foxtemas_javascripts', 100);
?>