<?php 
/**
 * Modelo de loop padrão para artigos/busca/arquivo/categoria
 *
 * @package Só Baixar
 * @version 1.0 
 * 
 */
?>

<!-- article -->
<article class="article article-default">
    
    <!-- header article -->
    <header class="header-article clearfix">
        
        <!-- name article -->
        <h2 class="name-article">
            <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                <?php the_title();?>
            </a>
        </h2>
        <!-- end name article -->

        <!-- infos article -->
        <div class="infos-article">
            
            <!-- rating -->
            <div class="rating-article">
                <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
            </div>
            <!-- end rating -->

            <!-- categories -->
            <div class="categories-article">
                <i class="fa fa-folder"> </i> <?php the_category( ' › ' ); ?> <?php edit_post_link( 'Editar', ' | ', '' ); ?>
            </div>
            <!-- end categories -->

        </div>
        <!-- end infos article -->

    </header>
    <!-- end header article -->

    <!-- entry -->
    <div class="entry clearfix">
        <?php the_content('Mais ››'); ?>
    </div>
    <!-- end entry -->
    
    <!-- footer article -->
    <footer class="footer-article clearfix">
        <!-- sharer article -->
        <div class="sharer-article">
            
            <!-- text sharer -->
            <span class="text-sharer-article">
                <i class="fa fa-comments"></i> <a href="<?php the_permalink();?>/#respond" title="Comente!"><span class="fb-comments-count" data-href="<?php the_permalink();?>">0</span> Comentários</a> | 
            </span>
            <!-- end text sharer -->
            
            <!-- buttons sharer -->
            <span class="buttons-sharer-article">
                
                <span class="btn-share">
                    <div class="fb-like" data-href="<?php the_permalink();?>" data-width="110" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                </span>
        
                <span class="btn-share">
                    <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink();?>" data-text="<?php the_title();?>" data-lang="pt">Tweetar</a>
                </span>
        
                <span class="btn-share">
                    <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&media=<?php foxtemas_thumbnail('large', true);?>&description=<?php foxtemas_resumo(get_the_content(), 200);?>" data-pin-do="buttonPin" data-pin-config="above"><img src="http://assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
                </span>
        
            </span>
            <!-- end buttons sharer -->
        
        </div>
        <!-- end sharer article -->
    </footer>
    <!-- end footer article -->

</article>
<!-- end article -->