jQuery.noConflict();
jQuery(document).ready(function ($) {
	/** 
    // ==========================================================================
    //   Superfish Menu
    // ==========================================================================
    **/
    jQuery('ul.sf-menu').superfish({
            delay:       1000,                            // one second delay on mouseout
            animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation
            speed:       'fast',                          // faster animation speed
            autoArrows:  false                            // disable generation of arrow mark-up
        });




    /** 
    // ==========================================================================
    //   Responsive menu
    // ==========================================================================
    **/
    jQuery("#nav-mobile .container").html(jQuery("#nav-main").html());
    jQuery("#nav-trigger span").click(function(event){
        if (jQuery("#nav-mobile ul").hasClass("expanded")) {
            jQuery("#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
            jQuery(this).removeClass("open");
            event.stopPropagation();
        } else {
            jQuery("#nav-mobile ul").addClass("expanded").slideDown(250);
            jQuery(this).addClass("open");
            event.stopPropagation();
        }
    });

    jQuery('html').click(function() {
        jQuery("#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
        jQuery("#nav-trigger span").removeClass("open");
    });

    jQuery('#nav-mobile').click(function() {
        event.stopPropagation();
    });




    /** 
    // ==========================================================================
    //   Scroll Selector
    // ==========================================================================
    **/
    jQuery(window).load(function() {

        jQuery("[data-scrollto]").click(function() {

            var seletor = jQuery(this).attr("data-scrollto");

            function scroll_section() {

                jQuery('html, body').animate({
                    scrollTop: jQuery("#"+seletor).offset().top
                }, 500);

            }

            setTimeout(scroll_section, 100);

        });
    });



    /** 
    // ==========================================================================
    //   Center Image min
    // ==========================================================================
    **/
    var window_width = jQuery(window).width();

    /**
     * Se tamanho do browser menor ou igual a 420px
     */
    if(window_width <= 420) {
        /**
         * Para cada imagem da class .entry executar função
         */
         jQuery('.entry img').each(function() {

            /**
             * Dimensão da imagem
             * @type {int}
             */
             var img_width = jQuery(this).width();

            /**
             * Se imagem maior ou igual a 210 executar função
             * @param  {Number} img_width > Dimensão da imagem
             * 
             */
             if(img_width >= 210) {
                jQuery(this).removeClass('alignleft');
                jQuery(this).removeClass('alignright');
                jQuery(this).addClass('aligncenter');
                jQuery(this).css('margin-bottom', '15px');
            }

        });
    }

    /** 
    // ==========================================================================
    //   Tabs
    // ==========================================================================
    **/
    var TabBlock = {
        s: {
            animLen: 200
        },

        init: function() {
            TabBlock.bindUIActions();
            TabBlock.hideInactive();
        },

        bindUIActions: function() {
            $('.tabBlock-tabs').on('click', '.tabBlock-tab', function(){
                TabBlock.switchTab($(this));
            });
        },

        hideInactive: function() {
            var $tabBlocks = $('.tabBlock');

            $tabBlocks.each(function(i) {
                var 
                $tabBlock = $($tabBlocks[i]),
                $panes = $tabBlock.find('.tabBlock-pane'),
                $activeTab = $tabBlock.find('.tabBlock-tab.is-active');

                $panes.hide();
                $($panes[$activeTab.index()]).show();
            });
        },

        switchTab: function($tab) {
            var $context = $tab.closest('.tabBlock');

            if (!$tab.hasClass('is-active')) {
                $tab.siblings().removeClass('is-active');
                $tab.addClass('is-active');

                TabBlock.showPane($tab.index(), $context);
            }
        },

        showPane: function(i, $context) {
            var $panes = $context.find('.tabBlock-pane');

        // Normally I'd frown at using jQuery over CSS animations, but we can't transition between unspecified variable heights, right? If you know a better way, I'd love a read it in the comments or on Twitter @johndjameson
        $panes.slideUp(TabBlock.s.animLen);
            $($panes[i]).slideDown(TabBlock.s.animLen);
        }
    };

    $(function() {
        TabBlock.init();
    });

    /** 
    // ==========================================================================
    //   Form relatório
    // ==========================================================================
    **/
    jQuery('.abrir-relatorio').click(function(event) {
        jQuery('.form-relatorio').slideDown();
        event.stopPropagation();
    });

    jQuery('.form-relatorio').click(function(event) {
        event.stopPropagation();
    });

    jQuery('html').click(function(event) {
        jQuery('.form-relatorio').slideUp();
    });
});