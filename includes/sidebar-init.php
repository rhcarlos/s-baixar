<?php 
// ==========================================================================
//   File Security Check
// ==========================================================================// 
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'Você não tem permissão suficiente para acessar esse arquivo!' );
}



/** 
// ==========================================================================
//   Registra Sidebars
// ==========================================================================
**/

//   Sidebars Lateral 
// ==========================================================================
register_sidebar(array(
    'name' => "Sidebar Grande #1",
    'id' => 'sidebar-big-one',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '<div class="clearfix"></div></div></li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3><div class="sidebar-content">',
));

register_sidebar(array(
    'name' => "Sidebar Grande #2",
    'id' => 'sidebar-big-two',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '<div class="clearfix"></div></div></li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3><div class="sidebar-content">',
));

register_sidebar(array(
    'name' => "Sidebar Pequena Esquerda",
    'id' => 'sidebar-small-left',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '<div class="clearfix"></div></div></li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3><div class="sidebar-content">',
));


register_sidebar(array(
    'name' => "Sidebar Pequena Direita",
    'id' => 'sidebar-small-right',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '<div class="clearfix"></div></div></li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3><div class="sidebar-content">',
));


//   Sidebars Rodapé 
// ==========================================================================
register_sidebar(array(
    'name' => "Sidebar Rodapé Esquerdo",
    'id' => 'sidebar-footer-left',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '<div class="clearfix"></div></div></li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3><div class="sidebar-content">',
));


register_sidebar(array(
    'name' => "Sidebar Rodapé Centro",
    'id' => 'sidebar-footer-center',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '<div class="clearfix"></div></div></li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3><div class="sidebar-content">',
));

register_sidebar(array(
    'name' => "Sidebar Rodapé Direito",
    'id' => 'sidebar-footer-right',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '<div class="clearfix"></div></div></li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3><div class="sidebar-content">',
));

?>