<?php
// Load plugin
if (file_exists(dirname(__FILE__).'/acf.php')) {
    require_once( dirname(__FILE__).'/acf.php' );
}

// Define path metas plugin
if (file_exists(dirname(__FILE__).'/path-config.php')) {
    require_once( dirname(__FILE__).'/path-config.php' );
}

// Extension acf font awesome
if (file_exists(dirname(__FILE__).'/extensions/advanced-custom-fields-font-awesome/acf-font-awesome.php')) {
    require_once( dirname(__FILE__).'/extensions/advanced-custom-fields-font-awesome/acf-font-awesome.php' );
}