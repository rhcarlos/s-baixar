<?php get_header(); ?>


<!-- section wrap -->
<section class="section-wrap">
    
    <!-- bg wrap -->
    <div class="bg-wrap clearfix">
        
        <!-- container -->
        <div class="container">
            
            <!-- row -->
            <div class="row">
                
                <!-- left content -->
                <div class="col-xs-12 col-sm-8 col-md-7">
                    <div class="left-content" role="main">
                        
                        <?php if (have_posts()) : ?>

                            <?php while (have_posts()) : the_post(); ?>
                            
                                <?php get_template_part( 'loops/loop', 'default' ); ?>

                            <?php endwhile; ?>
                            
                                <?php if( function_exists('wp_pagenavi') ) { ?>
                                    <!-- pagenavi -->
                                    <div class="pagenavi">
                                        <?php wp_pagenavi(); ?>
                                    </div>
                                    <!-- end pagenavi -->
                                <?php } else { ?>
                                    <?php if (function_exists('foxtemas_pagenavi')) foxtemas_pagenavi(); ?>
                                <?php } ?> 

                        <?php else : ?>
                        
                            <!-- article -->
                            <article class="article article-default">
                                
                                <!-- header article -->
                                <header class="header-article clearfix">
                                    
                                    <!-- name article -->
                                    <h1 class="name-article text-center">
                                        <?php if($foxtemas_options['search_404_title']) : ?>
                                            <?php echo $foxtemas_options['search_404_title']; ?>
                                        <?php else : ?>
                                            <i class="fa fa-search"></i> Busca não encontrada
                                        <?php endif; ?>
                                    </h1>
                                    <!-- end name article -->

                                </header>
                                <!-- end header article -->

                                <!-- entry -->
                                <div class="entry clearfix">

                                <?php if(trim($foxtemas_options['search_404_content'])) : ?>
                                    <?php echo wpautop($foxtemas_options['search_404_content']); ?>
                                <?php else : ?>
                                    <p class="text-center">
                                        <?php if($foxtemas_options['logo_header']['url']) : ?>
                                            <img src="<?php echo $foxtemas_options['logo_header']['url'];?>" alt="<?php bloginfo('name');?>">
                                        <?php else : ?>
                                            <img src="<?php echo FOXTEMAS_THEME_DIR . 'images/logo.png';?>" alt="<?php bloginfo('name');?>">
                                        <?php endif; ?>
                                    </p>

                                    <p class="text-center">
                                        Desculpe, mas a página que você tentou acessar não existe. <br>
                                        Ela pode ter sido movida, ou removida do site. Tente fazer uma busca novamente.
                                    </p>
                                <?php endif; ?>


                                </div>
                                <!-- end entry -->
                                
                            </article>
                            <!-- end article -->

                        <?php endif; ?>

                    </div>
                </div>
                <!-- end left content -->

                <!-- rigt content -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-md-offset-1">
                    <aside class="right-content">
                        <?php get_sidebar( 'sidebar' ); ?>
                    </aside>
                </div>
                <!-- end right content -->
                
            </div>
            <!-- end row -->

        </div>
        <!-- end container -->

    </div>
    <!-- end bg wrap -->

</section>
<!-- end section wrap -->
<?php get_footer();?>