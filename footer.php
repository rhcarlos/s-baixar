<?php  global $foxtemas_options; ?>

<!-- footer -->
<footer class="footer" role="contentinfo">
    
    <div class="container">
        
        <!-- row -->
        <div class="row">
            
            <!-- col left -->
            <div class="col-xs-12 col-sm-4">
                <!-- widget footer -->
                <div class="sidebar-footer">
                    
                    <ul>            
                        <?php dynamic_sidebar('sidebar-footer-left');?>
                    </ul>
                
                </div>
                <!-- end widget footer -->
            </div>
            <!-- end col left -->

            <!-- col center -->
            <div class="col-xs-12 col-sm-4">
                <!-- widget footer -->
                <div class="sidebar-footer">
                    
                    <ul>            
                        <?php dynamic_sidebar('sidebar-footer-center');?>
                    </ul>

                </div>
                <!-- end widget footer -->
            </div>
            <!-- end col center -->

            <!-- col right -->
            <div class="col-xs-12 col-sm-4">
                <!-- widget footer -->
                <div class="sidebar-footer sidebar-footer-last">
                    
                    <ul>            
                        <?php dynamic_sidebar('sidebar-footer-right');?>
                    </ul>

                </div>
                <!-- end widget footer -->
            </div>
            <!-- end col right -->
        
        </div>
        <!-- end row -->

    </div>

</footer>
<!-- end footer -->

<!-- copyright -->
<div class="copyright">
    
    <div class="container">
        
        <!-- copy text -->
        <div class="copy-text">
            <?php if($foxtemas_options['texto_rodape']) : ?>
                <?php echo $foxtemas_options['texto_rodape']; ?>
            <?php else : ?>
                ©2015 - Só Baixar | Downloads Grátis
            <?php endif; ?>
        </div>
        <!-- end copy text -->

        <!-- dev -->
        <div class="dev-theme">
            Desenvolvido por <a href="http://www.inspiradora.com.br" target="_blank" title="Agência Inspiradora">Agência Inspiradora</a>
        </div>
        <!-- end dev -->

    </div>

</div>
<!-- end copyright -->

<?php wp_footer();  ?>

<!-- facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4&appId=402523586623781";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- twitter -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<?php echo $foxtemas_options['tracking_code']; ?>

</body>
</html>

