<?php 
/**
 * Modelo de loop padrão para single
 *
 * @package Só Baixar
 * @version 1.0 
 * 
 */

/**
 * Panel options push
 */
global $foxtemas_options;

?>

<!-- article -->
<article class="article article-single">
    
    <!-- header article -->
    <header class="header-article clearfix">
        
        <!-- name article -->
        <h1 class="name-article">
            <?php the_title();?>
        </h1>
        <!-- end name article -->

        <!-- infos article -->
        <div class="infos-article">
            
            <!-- rating -->
            <div class="rating-article">
                <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
            </div>
            <!-- end rating -->

            <!-- categories -->
            <div class="categories-article">
                <span style="float: left; margin-right: 15px;">Por <?php the_author_posts_link();?></span>

                <div class="clearfix visible-xs" style="margin-bottom: 10px; margin-top: 10px;"></div>

                <span style="float: left; margin: 0 15px 0 0;">
                    <div class="fb-like" data-href="<?php the_permalink();?>" data-width="110" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                </span>

                <span style="float: left; margin-right: 15px;">
                    <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink();?>" data-text="<?php the_title();?>" data-lang="pt">Tweetar</a>
                </span>

                <span style="float: left;">
                    <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&media=<?php foxtemas_thumbnail('large', true);?>&description=<?php foxtemas_resumo(get_the_content(), 200);?>" data-pin-do="buttonPin" data-pin-config="above"><img src="http://assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
                </span>
            </div>
            <!-- end categories -->

        </div>
        <!-- end infos article -->

    </header>
    <!-- end header article -->

    <!-- entry -->
    <div class="entry clearfix">
        <?php the_content('[ + ] Expandir Postagem ››'); ?>
    </div>
    <!-- end entry -->
    
    <!-- categories single -->
    <div class="categories-single">        
        <i class="fa fa-folder"> </i> <?php the_category( ' › ' ); ?>  <?php edit_post_link( 'Editar', ' | ', '' ); ?>
    </div>
    <!-- end categories single -->

    <!-- footer article -->
    <footer class="footer-article-single clearfix">
        <strong><i class="fa fa-exclamation-circle"></i> Encontrou algum problema? <a href="javascript:void(0);" title="Envie um relatório de erro!" class="abrir-relatorio">Envie um relatório de erro!</a></strong>

        <!-- form relatorio -->
        <div class="form-relatorio">
            <?php if($foxtemas_options['relatorio_erro']) : ?>
                <?php echo do_shortcode($foxtemas_options['relatorio_erro']); ?>
            <?php else : ?>
                Nenhum formulário configurado no painel fox temas
            <?php endif; ?>
        </div>
        <!-- end form relatorio -->
    </footer>
    <!-- end footer article -->

</article>
<!-- end article -->

<?php if(trim($foxtemas_options['ads_relacionados'])) : ?>
    <!-- ads related -->
    <div class="ads-related">
        <?php echo $foxtemas_options['ads_relacionados']; ?>
    </div>
    <!-- end ads related -->
<?php endif; ?>


<?php foxtemas_relacionados(); ?>

<!-- comentarios -->
<div class="comentarios" id="respond">
    
    <span class="title-heading">
        Comentários
    </span>

    <div class="fb-comments" data-href="<?php the_permalink();?>" data-width="100%" data-numposts="15"></div>

</div>
<!-- end comentarios -->