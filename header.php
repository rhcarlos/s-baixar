<?php global $foxtemas_options; ?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta property="fb:app_id" content="402523586623781" />

<title><?php wp_title('|', true, 'right'); bloginfo('name');?></title>

<?php 
/* Head WP default itens */
wp_head(); ?>

<?php 
/* List link archives wordpress */
wp_get_archives('type=monthly&format=link') ?>

<?php foxtemas_favicons(); ?>

<?php foxtemas_custom_css(); ?>

<?php echo $foxtemas_options['before_head']; ?>

<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>

<!-- nav mobile -->
<div id="nav-mobile"><div class="container"></div></div>
<!-- end nav mobile -->

<div class="container" style="position: relative;">
    <!-- nav trigger -->
    <div id="nav-trigger">
        <span><i class="fa fa-navicon"></i><i class="fa fa-close"></i></span>
    </div>
    <!-- end nav trigger -->
</div>


<!-- header -->
<header class="header" role="banner">

    <div class="container">
        
        <!-- logo header -->
        <div class="logo-header">
            <?php if(is_home()) : ?>
                <h1 style="margin: 0; padding: 0;">
                    <a href="<?php bloginfo('url');?>" title="<?php bloginfo('name');?>">
                        <?php if($foxtemas_options['logo_header']['url']) : ?>
                            <img src="<?php echo $foxtemas_options['logo_header']['url'];?>" alt="<?php bloginfo('name');?>">
                        <?php else : ?>
                            <img src="<?php echo FOXTEMAS_THEME_DIR . 'images/logo.png';?>" alt="<?php bloginfo('name');?>">
                        <?php endif; ?>
                    </a>
                </h1>
            <?php else : ?>
                <strong>
                    <a href="<?php bloginfo('url');?>" title="<?php bloginfo('name');?>">
                        <?php if($foxtemas_options['logo_header']['url']) : ?>
                            <img src="<?php echo $foxtemas_options['logo_header']['url'];?>" alt="<?php bloginfo('name');?>">
                        <?php else : ?>
                            <img src="<?php echo FOXTEMAS_THEME_DIR . 'images/logo.png';?>" alt="<?php bloginfo('name');?>">
                        <?php endif; ?>
                    </a>
                </strong>
            <?php endif; ?>
        </div>
        <!-- end logo header -->

        <!-- navigation -->
        <nav class="navigation-top" id="nav-main" role="navigation">
            
            <ul class="">
                <?php $args = array(
                    'container'       => '',
                    'fallback_cb'     => '', 
                    'echo'            => true,
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '%3$s',
                    'theme_location'     => 'menu_topo',
                    // 'walker' => 'rc_scm_walker',
                    // 'depth' => 2,
                );
                wp_nav_menu($args);  ?>
            </ul>

        </nav>
        <!-- end navigation -->

    </div>
    
</header>
<!-- end header -->

<!-- breadcrumb and text -->
<section class="section-breadcrumb">
    <div class="bg-breadcrumb">

        <div class="container">  

            <?php if(is_home() || is_front_page()) { ?>

                <?php if(trim($foxtemas_options['texto_topo'])) {?> 

                    <!-- breadcrumb -->
                    <div class="breadcrumb">
                        <?php echo $foxtemas_options['texto_topo']; ?>
                    </div>
                    <!-- end breadcrumb -->

                <?php } ?>

            <?php } else { ?>

                <!-- breadcrumb -->
                <div class="breadcrumb">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('','');
                    } else {
                        echo foxtemas_breadcrumb();
                    } ?>
                </div>
                <!-- end breadcrumb -->

            <?php } ?>

        </div>

    </div>
</section>
<!-- end breadcrumb and text -->


<?php if(trim($foxtemas_options['ads_header']) != '') : ?>
    <!-- ads -->
    <section class="section-ads-header clearfix">

        <div class="bg-ads">
            
            <div class="container">
                
                <div class="ads-header">
                    <?php echo $foxtemas_options['ads_header']; ?>
                </div>

            </div>
            
        </div>

    </section>
    <!-- end ads -->
<?php endif; ?>