<?php
/**
 * Funções shortcodes
 */


/**
 * Collapse séries
 */
function foxtemas_collapse_series() {

    // global infos post
    global $post;

    ob_start();

    // count
    $count_section = 1;

    if( have_rows('series') ): ?>

        <div class="clearfix"></div>

        <!-- panel group -->
        <div class="panel-group series-panel" id="accordion-<?php the_ID();?>" role="tablist" aria-multiselectable="true">

            <?php while ( have_rows('series') ) : the_row(); ?>

                <div class="panel">
                    <div id="collapse-heading-<?php echo $count_section;?>-post-<?php the_ID();?>" class="series-panel-heading" role="tab">
                        <h4 class="series-panel-title">
                            <a role="button" data-toggle="collapse" data-scrollto="collapse-heading-<?php echo $count_section;?>-post-<?php the_ID();?>" class="<?php if($count_section != 1) : echo 'collapsed'; endif; ?>" data-parent="#accordion-<?php the_ID();?>" href="#collapse-<?php echo $count_section;?>-post-<?php the_ID();?>" aria-expanded="<?php if($count_section == 1) : echo 'true'; else : echo 'false'; endif; ?>">
                                <?php the_sub_field('nome_secao'); ?>
                            </a>
                        </h4>
                    </div>

                    <div id="collapse-<?php echo $count_section;?>-post-<?php the_ID();?>" class="panel-collapse collapse <?php if($count_section == 1) : echo 'in'; endif; ?>" role="tabpanel">
                        <div class="series-panel-content">
                            <?php the_sub_field('conteudo_secao'); ?>
                        </div>
                    </div>
                </div>

            <?php $count_section++; endwhile; ?>

        </div>
        <!-- end panel group -->

    <?php else : endif;

    return ob_get_clean();

}
add_shortcode( 'series', 'foxtemas_collapse_series' );

?>