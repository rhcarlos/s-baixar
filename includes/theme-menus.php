<?php 
// ==========================================================================
//   File Security Check
// ==========================================================================// 
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'Você não tem permissão suficiente para acessar esse arquivo!' );
}



/** 
// ==========================================================================
//   Posições Menu
// ==========================================================================
**/
if (function_exists('register_nav_menus')) {
	register_nav_menus(array(
		'menu_topo' => 'Menu Topo',
	));
}
add_theme_support('menus');


?>