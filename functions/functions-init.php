<?php
/**
 * 
 * Arquivos para configurações de pastas e plugins incluidos ao tema
 * 
 * 
 * @package Fox Temas Framework
 * @author Rhuan Carlos <rhcarlosweb@gmail.com>
 * @version: 2.0
 * 
 */


// File Security Check
if ( ! defined( 'ABSPATH' ) ) exit;

/*

	- FOXTEMAS_FUNCTIONS - Pasta functions
	- FOXTEMAS_THEME_PATH - Pasta theme
	- FOXTEMAS_THEME_DIR - URL Pasta theme
	- FOXTEMAS_PANEL_PATH - Pasta panel theme
	- FOXTEMAS_PANEL_DIR - URL panel theme
	- FOXTEMAS_METAS_PATH - Pasta ACF theme
	- FOXTEMAS_METAS_DIR - URL ACF theme

*/


// ==========================================================================
//   Define chamadas
// ==========================================================================
define('FOXTEMAS_FUNCTIONS', trailingslashit( get_template_directory().'/functions') );			//	Pasta functions

define('FOXTEMAS_THEME_PATH', trailingslashit(TEMPLATEPATH));	// Pasta theme
define('FOXTEMAS_THEME_DIR', trailingslashit(get_template_directory_uri()));	// URL pasta theme


/* Redux */
define('FOXTEMAS_PANEL_PATH', trailingslashit( get_template_directory().'/functions/admin' ) );		// Pasta redux
define('FOXTEMAS_PANEL_DIR', trailingslashit( get_template_directory_uri().'/functions/admin' ) );	// URL pasta redux

/* ACF */
define('FOXTEMAS_METAS_PATH', trailingslashit( get_template_directory().'/functions/metas' ) );	 // Pasta ACF
define('FOXTEMAS_METAS_DIR', trailingslashit( get_template_directory_uri().'/functions/metas' ) );  // URL pasta ACF



// ==========================================================================
//   Load Frameworks required
// ==========================================================================
require_once(FOXTEMAS_PANEL_PATH . 'admin-init.php');				// Redux
require_once(FOXTEMAS_METAS_PATH . 'metas-init.php');				// ACF


?>